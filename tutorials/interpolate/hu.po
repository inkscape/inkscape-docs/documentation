# Translators:
# Somogyvári Róbert <cr04ch at gmail>, 2009.
# Arpad Biro <biro.arpad gmail>, 2009.
# Balázs Meskó <meskobalazs@mailbox.org>, 2018
# Gyuris Gellért <jobel@ujevangelizacio.hu>, 2018, 2019, 2020, 2021, 2023
#
msgid ""
msgstr ""
"Project-Id-Version: Inkscape tutorial: Interpolate\n"
"POT-Creation-Date: 2023-07-14 16:46+0200\n"
"PO-Revision-Date: 2023-07-01 20:31+0000\n"
"Last-Translator: Gyuris Gellért <jobel@ujevangelizacio.hu>, 2023\n"
"Language-Team: Hungarian (https://app.transifex.com/fsf-hu/teams/77907/hu/)\n"
"Language: hu\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#. Put one translator per line, in the form NAME <EMAIL>, YEAR1, YEAR2
msgctxt "_"
msgid "translator-credits"
msgstr ""
"Somogyvári Róbert <cr04ch at gmail>, 2008\n"
"Gyuris Gellért <jobel at ujevangelizacio.hu>, 2018, 2019, 2020, 2021, 2023"

#. (itstool) path: articleinfo/title
#: tutorial-interpolate.xml:6
msgid "Interpolate"
msgstr "Interpolálás"

#. (itstool) path: articleinfo/subtitle
#: tutorial-interpolate.xml:7
msgid "Tutorial"
msgstr "Ismertető"

#. (itstool) path: abstract/para
#: tutorial-interpolate.xml:11
msgid "This document explains how to use Inkscape's Interpolate extension"
msgstr "Ez a dokumentum az Inkscape Interpolálás effektusát ismerteti."

#. (itstool) path: sect1/title
#: tutorial-interpolate.xml:16
msgid "Introduction"
msgstr "Bevezetés"

#. (itstool) path: sect1/para
#: tutorial-interpolate.xml:17
msgid ""
"Interpolate does a <firstterm>linear interpolation</firstterm> between two "
"or more selected paths. It basically means that it “fills in the gaps” "
"between the paths and transforms them according to the number of steps given."
msgstr ""
"Az Interpolálás effektus <firstterm>lineáris interpoláció</firstterm>t hajt "
"végre két vagy több útvonal között. Alapvetően ez annyit jelent, hogy az "
"útvonalak közti „területet betölti” megadott számú, a kezdő útvonalból "
"fokozatosan a másik útvonalba átalakuló formákkal."

#. (itstool) path: sect1/para
#: tutorial-interpolate.xml:22
msgid ""
"To use the Interpolate extension, select the paths that you wish to "
"transform, and choose <menuchoice><guimenu>Extensions</"
"guimenu><guisubmenu>Generate From Path</guisubmenu><guimenuitem>Interpolate "
"Between Paths</guimenuitem></menuchoice> from the menu."
msgstr ""
"Az interpolálás kiterjesztés használatához jelöljön ki egy útvonalat, "
"amelyet transzformálni kíván, majd válassza a  "
"<menuchoice><guimenu>Kiterjesztések</guimenu><guisubmenu>Létrehozás útvonal "
"alapján</guisubmenu><guimenuitem>Interpolálás útvonalak között…</"
"guimenuitem></menuchoice> parancsot a menüből."

#. (itstool) path: sect1/para
#: tutorial-interpolate.xml:27
msgid ""
"Before invoking the extension, the objects that you are going to transform "
"need to be <emphasis>paths</emphasis>. This is done by selecting the object "
"and using <menuchoice><guimenu>Path</guimenu><guimenuitem>Object to Path</"
"guimenuitem></menuchoice> or <keycombo><keycap function=\"control\">Ctrl</"
"keycap><keycap function=\"shift\">Shift</keycap><keycap>C</keycap></"
"keycombo>. If your objects are not paths, the extension will do nothing."
msgstr ""
"A kiterjesztés meghívása előtt azonban az objektumokat <emphasis>útvonalak</"
"emphasis>ká kell alakítani, ha nem azok volnának. Ezt kijelölés után az "
"<menuchoice><guimenu>Útvonal</guimenu><guimenuitem>Objektum átalakítása "
"útvonallá</guimenuitem></menuchoice> vagy a <keycombo><keycap "
"function=\"control\">Ctrl</keycap> <keycap function=\"shift\">Shift</keycap> "
"<keycap>C</keycap></keycombo> paranccsal teheti meg. Útvonaltól eltérő "
"objektumokra a kiterjesztés nincs hatással."

#. (itstool) path: sect1/title
#: tutorial-interpolate.xml:36
msgid "Interpolation between two identical paths"
msgstr "Két azonos útvonal közötti interpolálás"

#. (itstool) path: sect1/para
#: tutorial-interpolate.xml:37
msgid ""
"The simplest use of the Interpolate extension is to interpolate between two "
"paths that are identical. When the extension is called, the result is that "
"the space between the two paths is filled with duplicates of the original "
"paths. The number of steps defines how many of these duplicates are placed."
msgstr ""
"Az interpolálás kiterjesztés használatának legegyszerűbb módja a két azonos "
"útvonal közötti interpolálás. A kiterjesztés meghívásakor az útvonalak közti "
"területre az eredeti útvonalak másolatai kerülnek. A lépések száma határozza "
"meg a másolatok darabszámát."

#. (itstool) path: sect1/para
#: tutorial-interpolate.xml:42 tutorial-interpolate.xml:75
msgid "For example, take the following two paths:"
msgstr "Az alábbi két útvonalat például véve:"

#. (itstool) path: sect1/para
#: tutorial-interpolate.xml:52
msgid ""
"Now, select the two paths, and run the Interpolate extension with the "
"settings shown in the following image."
msgstr ""
"Jelölje ki őket, és alkalmazza rájuk az Interpolálás kiterjesztést, az "
"alábbi ábrán látható beállításokkal."

#. (itstool) path: sect1/para
#: tutorial-interpolate.xml:62
msgid ""
"As can be seen from the above result, the space between the two circle-"
"shaped paths has been filled with 6 (the number of interpolation steps) "
"other circle-shaped paths. Also note that the extension groups these shapes "
"together."
msgstr ""
"Amint látja, a két kör alakú útvonal közötti területre 6 (az interpolációs "
"lépések száma) újabb kör alakú útvonal került. Észreveheti, hogy a "
"kiterjesztés csoportba is foglalja ezeket az útvonalakat."

#. (itstool) path: sect1/title
#: tutorial-interpolate.xml:69
msgid "Interpolation between two different paths"
msgstr "Két különböző útvonal közötti interpolálás"

#. (itstool) path: sect1/para
#: tutorial-interpolate.xml:70
msgid ""
"When interpolation is done on two different paths, the program interpolates "
"the shape of the path from one into the other. The result is that you get a "
"morphing sequence between the paths, with the regularity still defined by "
"the Interpolation Steps value."
msgstr ""
"Két eltérő alakú útvonal közötti interpoláláskor a közbenső útvonalak "
"formája az egyik eredeti útvonal formájából indulva, az Interpolációs "
"lépések paraméter által meghatározott szabályszerűséggel alakul át a másik "
"eredeti útvonal formájába."

#. (itstool) path: sect1/para
#: tutorial-interpolate.xml:85
msgid ""
"Now, select the two paths, and run the Interpolate extension. The result "
"should be like this:"
msgstr ""
"Jelölje ki őket, és alkalmazza megint az Interpolálás kiterjesztést. Ilyen "
"eredményt kell kapnia:"

#. (itstool) path: sect1/para
#: tutorial-interpolate.xml:95
msgid ""
"As can be seen from the above result, the space between the circle-shaped "
"path and the triangle-shaped path has been filled with 6 paths that progress "
"in shape from one path to the other."
msgstr ""
"Láthatja, hogy ebben az esetben a kör és a háromszög közötti területre 6 "
"olyan útvonal került, melyek alakja fokozatosan alakul át egyik formából a "
"másikba."

#. (itstool) path: sect1/para
#: tutorial-interpolate.xml:99
msgid ""
"When using the Interpolate extension on two different paths, the "
"<emphasis>position</emphasis> of the starting node of each path is "
"important. To find the starting node of a path, select the path, then choose "
"the Node Tool so that the nodes appear and press <keycap>TAB</keycap>. The "
"first node that is selected is the starting node of that path."
msgstr ""
"Az interpolálás kiterjesztést két különböző alakú útvonalra alkalmazva az "
"útvonalak kezdő csomópontjának <emphasis>pozíció</emphasis>ja meghatározó. A "
"kezdő csomópontot úgy találhatja meg, hogy kijelöl egy útvonalat, majd a "
"Csomópont eszközre vált, és lenyomja a <keycap>TAB</keycap> billentyűt, "
"amely először mindig a kezdő csomópontot jelöli ki."

#. (itstool) path: sect1/para
#: tutorial-interpolate.xml:105
msgid ""
"See the image below, which is identical to the previous example, apart from "
"the node points being displayed. The node that is green on each path is the "
"starting node."
msgstr ""
"Az alábbi képen az előbbi példa látható, de bejelöltük rajta a csomópontokat "
"is. Mindkét útvonalon zöld szín mutatja a kezdő csomópont helyét."

#. (itstool) path: sect1/para
#: tutorial-interpolate.xml:116
msgid ""
"The previous example (shown again below) was done with these nodes being the "
"starting node."
msgstr ""
"Az előző példán (alább újra láthatja) ilyen volt a kezdő csomópontok "
"helyzete."

#. (itstool) path: sect1/para
#: tutorial-interpolate.xml:126
msgid ""
"Now, notice the changes in the interpolation result when the triangle path "
"is mirrored so the starting node is in a different position:"
msgstr ""
"Az alábbi ábrán megfigyelheti, hogyan változik az interpolálás eredménye a "
"háromszög tükrözésével más pozícióba mozgatott kezdő csomópont hatására."

#. (itstool) path: sect1/title
#: tutorial-interpolate.xml:146
msgid "Interpolation Method"
msgstr "Interpolációs módszer"

#. (itstool) path: sect1/para
#: tutorial-interpolate.xml:147
msgid ""
"One of the parameters of the Interpolate extension is the Interpolation "
"Method. There are 2 interpolation methods implemented, and they differ in "
"the way that they calculate the curves of the new shapes. The choices are "
"either <guilabel>Split paths into segments of equal lengths</guilabel> or "
"<guilabel>Discard extra nodes of longer path</guilabel>."
msgstr ""
"Az Interpolálás kiterjesztés egyik paramétere az Interpolációs módszer. A "
"programban két fajtája van megvalósítva, melyek az új útvonalak alakját "
"eltérő módon számolják ki. A választási lehetőségek: vagy az "
"<guilabel>útvonalak felosztása egyenlő hosszú szakaszokra</guilabel>, vagy "
"<guilabel>a hosszabb útvonal extra csomópontjainak eldobása</guilabel>."

#. (itstool) path: sect1/para
#: tutorial-interpolate.xml:153
msgid ""
"In the examples above, we used the first Interpolation Method (Split paths), "
"and the result was:"
msgstr ""
"Az alábbi példában az első interpolációs módszer van alkalmazva )útvonal "
"felosztása), ami ilyen eredményt ad:"

#. (itstool) path: sect1/para
#: tutorial-interpolate.xml:163
msgid "Now compare this to Interpolation Method 2 (Ignore nodes):"
msgstr ""
"Összevetésképpen íme a második interpolálási módszer (csomópontok figyelmen "
"kívül hagyása):"

#. (itstool) path: sect1/para
#: tutorial-interpolate.xml:173
msgid ""
"The differences in how these methods calculate the numbers is beyond the "
"scope of this document, so simply try both, and use which ever one gives the "
"result closest to what you intend."
msgstr ""
"A két módszer számítási eljárása közti eltérés kifejtése meghaladná ezen "
"írás kereteit, ezért csak azt javasoljuk, hogy próbálja ki mindkettőt, és "
"használja mindig azt, amelyik az elvárásaihoz közelebbi eredményt adja."

#. (itstool) path: sect1/title
#: tutorial-interpolate.xml:179
msgid "Exponent"
msgstr "Kitevő"

#. (itstool) path: sect1/para
#: tutorial-interpolate.xml:180
msgid ""
"The <firstterm>exponent parameter</firstterm> controls the spacing between "
"steps of the interpolation. An exponent of 1 makes the spacing between the "
"copies all even."
msgstr ""
"A <firstterm>Kitevő</firstterm> paraméter az interpolációs lépések térközét "
"szabályozza. Ha a kitevő 1, az elemek között egyforma lesz a távolság."

#. (itstool) path: sect1/para
#: tutorial-interpolate.xml:184
msgid "Here is the result of another basic example with an exponent of 1."
msgstr "Az alábbi példa is 1 kitevővel készült:"

#. (itstool) path: sect1/para
#: tutorial-interpolate.xml:194
msgid "The same example with an exponent of 0.5:"
msgstr "Ugyanaz a példa, de most a kitevő értéke 0,5:"

#. (itstool) path: sect1/para
#: tutorial-interpolate.xml:204
msgid "with an exponent of 0.3:"
msgstr "A kitevő értéke 0,3:"

#. (itstool) path: sect1/para
#: tutorial-interpolate.xml:214
msgid "and with an exponent of 1.5:"
msgstr "Végül a kitevő értéke −1,5:"

#. (itstool) path: sect1/para
#: tutorial-interpolate.xml:224
msgid ""
"When dealing with exponents in the Interpolate extension, the "
"<emphasis>order</emphasis> that you select the objects is important. In the "
"examples above, the star-shaped path on the left was selected first, and the "
"hexagon-shaped path on the right was selected second."
msgstr ""
"Az interpolálás kiterjesztés használata közben a kitevő hatása szempontjából "
"lényeges, hogy milyen <emphasis>sorrend</emphasis>ben jelöli ki az "
"objektumokat. A fenti példákban a bal oldali csillag volt először kijelölve, "
"másodjára pedig a jobb oldali hatszög."

#. (itstool) path: sect1/para
#: tutorial-interpolate.xml:229
msgid ""
"View the result when the path on the right was selected first. The exponent "
"in this example was set to 0.5:"
msgstr ""
"A következő rajz úgy készült, hogy a jobb oldali útvonalat jelöltük ki "
"először. A kitevő ez esetben 0,5 volt:"

#. (itstool) path: sect1/title
#: tutorial-interpolate.xml:241
msgid "Duplicate Endpaths"
msgstr "Vég-útvonalak kettőzése"

#. (itstool) path: sect1/para
#: tutorial-interpolate.xml:242
msgid ""
"This parameter defines whether the group of paths that is generated by the "
"extension <emphasis>includes a copy</emphasis> of the original paths that "
"interpolate was applied on."
msgstr ""
"Ettől a paramétertől függ, hogy a kiterjesztés eredményeként létrejövő "
"útvonal-csoport <emphasis>tartalmazza-e egy-egy másolatát</emphasis> az "
"eredeti útvonalaknak is."

#. (itstool) path: sect1/title
#: tutorial-interpolate.xml:248
msgid "Interpolate Style"
msgstr "Stílus interpolálása"

#. (itstool) path: sect1/para
#: tutorial-interpolate.xml:249
msgid ""
"This parameter is one of the neat functions of the interpolate extension. It "
"tells the extension to attempt to change the style of the paths at each "
"step. So if the start and end paths are different colors, the paths that are "
"generated will incrementally change as well."
msgstr ""
"Ez az egyik leghasznosabb paramétere az Interpolálás kiterjesztésnek. Ha "
"engedélyezi, minden lépesben változik az útvonal stílusa is, így ha a "
"kiinduló útvonalak különböző színűek, akkor a létrehozott útvonalak színe is "
"lépésenként eltérő lesz."

#. (itstool) path: sect1/para
#: tutorial-interpolate.xml:254
msgid ""
"Here is an example where the Interpolate Style function is used on the fill "
"of a path:"
msgstr ""
"Ez a példa a Stílus interpolálása paraméter hatását mutatja az útvonalak "
"kitöltésére:"

#. (itstool) path: sect1/para
#: tutorial-interpolate.xml:264
msgid "Interpolate Style also affects the stroke of a path:"
msgstr "A Stílus interpolálása a körvonalra is hatással van:"

#. (itstool) path: sect1/para
#: tutorial-interpolate.xml:274
msgid ""
"Of course, the path of the start point and the end point does not have to be "
"the same either:"
msgstr "Természetesen a kezdő- és a vég-útvonal eltérő alakú is lehet:"

#. (itstool) path: sect1/title
#: tutorial-interpolate.xml:286
msgid "Using Interpolate to fake irregular-shaped gradients"
msgstr "Szabálytalan színátmenet interpolálás segítségével"

#. (itstool) path: sect1/para
#: tutorial-interpolate.xml:287
msgid ""
"At the time when gradient meshes were not implemented in Inkscape, it was "
"not possible to create a gradient other than linear (straight line) or "
"radial (round). However, it could be faked using the Interpolate extension "
"and Interpolate Style. A simple example follows — draw two lines of "
"different strokes:"
msgstr ""
"Amíg az Inkscape-ben nem voltak implementálva a színátmenetes hálók, addig "
"nem lehetett létrehozni más típusú színátmeneteket, csak lineárisat (egyenes "
"vonalút) vagy sugárirányút (kör alakút). Azonban az Interpolálás "
"kiterjesztéssel, a stílus interpolálása alkalmazásával utánozható volt "
"másféle színátmenet is. Következzen egy egyszerű példa – rajzoljon két "
"eltérő színű vonalat:"

#. (itstool) path: sect1/para
#: tutorial-interpolate.xml:299
msgid "And interpolate between the two lines to create your gradient:"
msgstr "Ezután interpolálással hozza létre közöttük a színátmenetet:"

#. (itstool) path: sect1/title
#: tutorial-interpolate.xml:311
msgid "Conclusion"
msgstr "Zárszó"

#. (itstool) path: sect1/para
#: tutorial-interpolate.xml:312
msgid ""
"As demonstrated above, the Inkscape Interpolate extension is a powerful "
"tool. This tutorial covers the basics of this extension, but experimentation "
"is the key to exploring interpolation further."
msgstr ""
"Amint a fentiekből látható, az Interpolálás az Inkscape egy nagyon hasznos "
"eszköze. Jelen ismertetőben ezen effektusnak csak az alapjait volt módunk "
"bemutatni, de kísérletezéssel további ismereteket szerezhet az "
"interpolálásról."

#. (itstool) path: Work/format
#: interpolate-f01.svg:49 interpolate-f02.svg:52 interpolate-f03.svg:49
#: interpolate-f04.svg:52 interpolate-f05.svg:49 interpolate-f07.svg:49
#: interpolate-f08.svg:49 interpolate-f09.svg:49 interpolate-f10.svg:49
#: interpolate-f11.svg:52 interpolate-f12.svg:52 interpolate-f13.svg:52
#: interpolate-f14.svg:52 interpolate-f15.svg:52 interpolate-f16.svg:49
#: interpolate-f17.svg:49 interpolate-f18.svg:49 interpolate-f19.svg:49
#: interpolate-f20.svg:52
msgid "image/svg+xml"
msgstr "image/svg+xml"

#. (itstool) path: text/tspan
#: interpolate-f02.svg:128 interpolate-f04.svg:119 interpolate-f11.svg:119
#, no-wrap
msgid "Exponent: 1.0"
msgstr "Kitevő: 1,0"

#. (itstool) path: text/tspan
#: interpolate-f02.svg:132 interpolate-f04.svg:123 interpolate-f11.svg:123
#, no-wrap
msgid "Interpolation Steps: 6"
msgstr "Interpolációs lépések: 6"

#. (itstool) path: text/tspan
#: interpolate-f02.svg:136 interpolate-f04.svg:127 interpolate-f11.svg:127
#, no-wrap
msgid "Interpolation Method: Split paths into segments of equal lengths"
msgstr "Interpolációs módszer: Útvonalak felosztása egyenlő hosszú szakaszokra"

#. (itstool) path: text/tspan
#: interpolate-f02.svg:140 interpolate-f04.svg:131 interpolate-f11.svg:131
#, no-wrap
msgid "Duplicate Endpaths: unchecked   "
msgstr "Vég-útvonalak kettőzése: nincs kiválasztva"

#. (itstool) path: text/tspan
#: interpolate-f02.svg:144 interpolate-f04.svg:135 interpolate-f11.svg:135
#, no-wrap
msgid "Interpolate Style: unchecked  "
msgstr "Stílus interpolálása: nincs kiválasztva"

#. (itstool) path: text/tspan
#: interpolate-f02.svg:148 interpolate-f04.svg:139 interpolate-f11.svg:139
#, no-wrap
msgid "Use Z-order: unchecked"
msgstr "Z-sorrend alkalmazása: nincs kiválasztva"
