msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2023-07-14 16:46+0200\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. Put one translator per line, in the form NAME <EMAIL>, YEAR1, YEAR2
msgctxt "_"
msgid "translator-credits"
msgstr ""

#. (itstool) path: articleinfo/title
#: tutorial-shapes.xml:6
msgid "Shapes"
msgstr ""

#. (itstool) path: articleinfo/subtitle
#: tutorial-shapes.xml:7
msgid "Tutorial"
msgstr ""

#. (itstool) path: abstract/para
#: tutorial-shapes.xml:11
msgid ""
"This tutorial covers the four shape tools: Rectangle, Ellipse, Star, and "
"Spiral. We will demonstrate the capabilities of Inkscape shapes and show "
"examples of how and when they could be used."
msgstr ""

#. (itstool) path: abstract/para
#: tutorial-shapes.xml:15
msgid ""
"Use <keycombo><keycap function=\"control\">Ctrl</keycap><keycap>Arrows</"
"keycap></keycombo>, <mousebutton role=\"mouse-wheel\">mousewheel</"
"mousebutton>, or <mousebutton role=\"middle-button-drag\">middle button "
"drag</mousebutton> to scroll the page down. For basics of object creation, "
"selection, and transformation, see the Basic tutorial in "
"<menuchoice><guimenu>Help</guimenu><guimenuitem>Tutorials</guimenuitem></"
"menuchoice>."
msgstr ""

#. (itstool) path: article/para
#: tutorial-shapes.xml:22
msgid ""
"Inkscape has four versatile <firstterm>shape tools</firstterm>, each tool "
"capable of creating and editing its own type of shapes. A shape is an object "
"which you can modify in ways unique to this shape type, using draggable "
"<firstterm>handles</firstterm> and numeric <firstterm>parameters</firstterm> "
"that determine the shape's appearance."
msgstr ""

#. (itstool) path: article/para
#: tutorial-shapes.xml:28
msgid ""
"For example, with a star you can alter the number of tips, their length, "
"angle, rounding, etc. — but a star remains a star. A shape is “less free” "
"than a simple path, but it's often more interesting and useful. You can "
"always convert a shape to a path (<keycombo><keycap "
"function=\"control\">Ctrl</keycap><keycap function=\"shift\">Shift</"
"keycap><keycap>C</keycap></keycombo>), but the reverse conversion is not "
"possible."
msgstr ""

#. (itstool) path: article/para
#: tutorial-shapes.xml:35
msgid ""
"The shape tools are <firstterm>Rectangle</firstterm>, <firstterm>Ellipse</"
"firstterm>, <firstterm>Star</firstterm>, and <firstterm>Spiral</firstterm>. "
"First, let's look at how shape tools work in general; then we'll explore "
"each shape type in detail."
msgstr ""

#. (itstool) path: sect1/title
#: tutorial-shapes.xml:41
msgid "General tips"
msgstr ""

#. (itstool) path: sect1/para
#: tutorial-shapes.xml:42
msgid ""
"A new shape is created by <mousebutton role=\"mouse-drag\">drag</"
"mousebutton>ging on canvas with the corresponding tool. Once the shape is "
"created (and so long as it is selected), it displays its handles as white "
"diamond, square or round marks (depending on the tools), so you can "
"immediately edit what you created by dragging these handles."
msgstr ""

#. (itstool) path: sect1/para
#: tutorial-shapes.xml:48
msgid ""
"All four kinds of shapes display their handles in all four shape tools as "
"well as in the Node tool (<keycap>N</keycap>). When you hover your "
"<mousebutton>mouse</mousebutton> over a handle, it tells you in the "
"statusbar what this handle will do when dragged or clicked with different "
"modifiers."
msgstr ""

#. (itstool) path: sect1/para
#: tutorial-shapes.xml:53
msgid ""
"Also, each shape tool displays its parameters in the <firstterm>Tool "
"Controls bar</firstterm> (which runs horizontally above the canvas). Usually "
"it has a few numeric entry fields and a button to reset the values to "
"defaults. When shape(s) of the current tool's native type are selected, "
"editing the values in the Controls bar changes the selected shape(s)."
msgstr ""

#. (itstool) path: sect1/para
#: tutorial-shapes.xml:59
msgid ""
"Any changes made to the Tool Controls are remembered and used for the next "
"object you draw with that tool. For example, after you change the number of "
"tips of a star, new stars will have this number of tips as well when drawn. "
"Moreover, even simply selecting a shape sends its parameters to the Tool "
"Controls bar and thus sets the values for newly created shapes of this type."
msgstr ""

#. (itstool) path: sect1/para
#: tutorial-shapes.xml:65
msgid ""
"When in a shape tool, selecting an object can be done by <mousebutton "
"role=\"click\">click</mousebutton>ing on it. <keycombo><keycap "
"function=\"control\">Ctrl</keycap><mousebutton role=\"click\">click</"
"mousebutton></keycombo> (select in group) and <keycombo><keycap "
"function=\"alt\">Alt</keycap><mousebutton role=\"click\">click</"
"mousebutton></keycombo> (select under) also work as they do in Selector "
"tool. <keycap>Esc</keycap> deselects."
msgstr ""

#. (itstool) path: sect1/title
#: tutorial-shapes.xml:74
msgid "Rectangles"
msgstr ""

#. (itstool) path: sect1/para
#: tutorial-shapes.xml:75
msgid ""
"A <firstterm>rectangle</firstterm> is the simplest but perhaps the most "
"common shape in design and illustration. Inkscape attempts to make creating "
"and editing rectangles as easy and convenient as possible."
msgstr ""

#. (itstool) path: sect1/para
#: tutorial-shapes.xml:79
msgid ""
"Switch to the Rectangle tool by pressing <keycap>R</keycap> or by clicking "
"its toolbar button. Draw a new rectangle alongside this blue one:"
msgstr ""

#. (itstool) path: sect1/para
#: tutorial-shapes.xml:90
msgid ""
"Then, without leaving the Rectangle tool, switch selection from one "
"rectangle to the other by clicking on them."
msgstr ""

#. (itstool) path: sect1/para
#: tutorial-shapes.xml:93
msgid "Rectangle drawing shortcuts:"
msgstr ""

#. (itstool) path: listitem/para
#: tutorial-shapes.xml:98
msgid ""
"With <keycap function=\"control\">Ctrl</keycap>, draw a square or an integer-"
"ratio (2:1, 3:1, etc) rectangle."
msgstr ""

#. (itstool) path: listitem/para
#: tutorial-shapes.xml:103 tutorial-shapes.xml:291
msgid ""
"With <keycap function=\"shift\">Shift</keycap>, draw around the starting "
"point as center."
msgstr ""

#. (itstool) path: sect1/para
#: tutorial-shapes.xml:108
msgid ""
"As you see, the selected rectangle (the just-drawn rectangle is always "
"selected) shows three adjustable handles in three of its corners and one "
"cross-shaped handle, for moving the rectangle, in the middle. In fact, there "
"are four corner handles, but two of them (in the top right corner) overlap "
"if the rectangle is not rounded. These two are the <firstterm>rounding "
"handles</firstterm>; the other two (top left and bottom right) are "
"<firstterm>resize handles</firstterm>."
msgstr ""

#. (itstool) path: sect1/para
#: tutorial-shapes.xml:115
msgid ""
"Let's look at the rounding handles first. Grab one of them and drag down. "
"All four corners of the rectangle become rounded, and you can now see the "
"second rounding handle — it stays in the original position in the corner. If "
"you want circular rounded corners, that is all you need to do. If you want "
"corners which are rounded farther along one side than along the other, you "
"can move that other handle leftwards."
msgstr ""

#. (itstool) path: sect1/para
#: tutorial-shapes.xml:121
msgid ""
"Here, the first two rectangles have circular rounded corners, and the other "
"two have elliptic rounded corners:"
msgstr ""

#. (itstool) path: sect1/para
#: tutorial-shapes.xml:131
msgid ""
"Still in the Rectangle tool, click on these rectangles to select, and "
"observe their rounding handles."
msgstr ""

#. (itstool) path: sect1/para
#: tutorial-shapes.xml:134
msgid ""
"Often, the radius and shape of the rounded corners must be constant within "
"the entire composition, even if the sizes of the rectangles are different "
"(think diagrams with rounded boxes of various sizes). Inkscape makes this "
"easy. Switch to the Selector tool; in its Tool Controls bar, there's a group "
"of four toggle buttons, the second from the left showing two concentric "
"rounded corners. This is how you control whether the rounded corners are "
"scaled when the rectangle is scaled or not."
msgstr ""

#. (itstool) path: sect1/para
#: tutorial-shapes.xml:141
msgid ""
"For example, here the original red rectangle is duplicated and scaled "
"several times, up and down, to different proportions, with the "
"“<guibutton>Scale rounded corners</guibutton>” button <emphasis>off</"
"emphasis>:"
msgstr ""

#. (itstool) path: sect1/para
#: tutorial-shapes.xml:152
msgid ""
"Note how the size and shape of the rounded corners is the same in all "
"rectangles, so that the roundings align exactly in the top right corner "
"where they all meet. All the dotted blue rectangles are obtained from the "
"original red rectangle just by scaling in Selector, without any manual "
"readjustment of the rounding handles."
msgstr ""

#. (itstool) path: sect1/para
#: tutorial-shapes.xml:157
msgid ""
"For a comparison, here is the same composition but now created with the "
"“<guibutton>Scale rounded corners</guibutton>” button <emphasis>on</"
"emphasis>:"
msgstr ""

#. (itstool) path: sect1/para
#: tutorial-shapes.xml:168
msgid ""
"Now the rounded corners are as different as the rectangles they belong to, "
"and there isn't a slightest agreement in the top right corner (zoom in to "
"see). This is the same (visible) result as you would get by converting the "
"original rectangle to a path (<keycombo><keycap function=\"control\">Ctrl</"
"keycap><keycap function=\"shift\">Shift</keycap><keycap>C</keycap></"
"keycombo>) and scaling it as path."
msgstr ""

#. (itstool) path: sect1/para
#: tutorial-shapes.xml:175
msgid "Here are the shortcuts for the rounding handles of a rectangle:"
msgstr ""

#. (itstool) path: listitem/para
#: tutorial-shapes.xml:180
msgid ""
"<mousebutton role=\"mouse-drag\">Drag</mousebutton> with <keycap "
"function=\"control\">Ctrl</keycap> to make the other radius the same "
"(circular rounding)."
msgstr ""

#. (itstool) path: listitem/para
#: tutorial-shapes.xml:186
msgid ""
"<keycombo><keycap function=\"control\">Ctrl</keycap><mousebutton "
"role=\"click\">click</mousebutton></keycombo> to make the other radius the "
"same without dragging."
msgstr ""

#. (itstool) path: listitem/para
#: tutorial-shapes.xml:192
msgid ""
"<keycombo><keycap function=\"shift\">Shift</keycap><mousebutton "
"role=\"click\">click</mousebutton></keycombo> to remove rounding."
msgstr ""

#. (itstool) path: sect1/para
#: tutorial-shapes.xml:198
msgid ""
"You may have noticed that the Rectangle tool's Controls bar shows the "
"horizontal (<firstterm>Rx</firstterm>) and vertical (<firstterm>Ry</"
"firstterm>) rounding radii for the selected rectangle and lets you set them "
"precisely using any length units. The <guimenuitem>Make corners sharp</"
"guimenuitem> button does what is says — removes rounding from the selected "
"rectangle(s)."
msgstr ""

#. (itstool) path: sect1/para
#: tutorial-shapes.xml:204
msgid ""
"An important advantage of these controls is that they can affect many "
"rectangles at once. For example, if you want to change all rectangles in the "
"layer, just do <keycombo><keycap function=\"control\">Ctrl</"
"keycap><keycap>A</keycap></keycombo> (<guimenuitem>Select All</guimenuitem>) "
"and set the parameters you need in the Controls bar. If any non-rectangles "
"are selected, they will be ignored — only rectangles will be changed."
msgstr ""

#. (itstool) path: sect1/para
#: tutorial-shapes.xml:211
msgid ""
"Now let's look at the resize handles of a rectangle. You might wonder, why "
"do we need them at all, if we can just as well resize the rectangle with "
"Selector?"
msgstr ""

#. (itstool) path: sect1/para
#: tutorial-shapes.xml:215
msgid ""
"The problem with Selector is that its notion of horizontal and vertical is "
"always that of the document page. By contrast, a rectangle's resize handles "
"scale it <emphasis>along that rectangle's sides</emphasis>, even if the "
"rectangle is rotated or skewed. For example, try to resize this rectangle "
"first with Selector and then with its resize handles in Rectangle tool:"
msgstr ""

#. (itstool) path: sect1/para
#: tutorial-shapes.xml:228
msgid ""
"Since the resize handles are two, you can resize the rectangle into any "
"direction or even move it along its sides. Resize handles always preserve "
"the rounding radii."
msgstr ""

#. (itstool) path: sect1/para
#: tutorial-shapes.xml:232
msgid "Here are the shortcuts for the resize handles:"
msgstr ""

#. (itstool) path: listitem/para
#: tutorial-shapes.xml:237
msgid ""
"<mousebutton role=\"mouse-drag\">Drag</mousebutton> with <keycap "
"function=\"control\">Ctrl</keycap> to snap to the sides or the diagonal of "
"the rectangle. In other words, <keycap function=\"control\">Ctrl</keycap> "
"preserves either width, or height, or the width/height ratio of the "
"rectangle (again, in its own coordinate system which may be rotated or "
"skewed)."
msgstr ""

#. (itstool) path: sect1/para
#: tutorial-shapes.xml:245
msgid ""
"Here is the same rectangle, with the gray dotted lines showing the "
"directions to which the resize handles stick when dragged with <keycap "
"function=\"control\">Ctrl</keycap> (try it):"
msgstr ""

#. (itstool) path: sect1/para
#: tutorial-shapes.xml:256
msgid ""
"By slanting and rotating a rectangle, then duplicating it and resizing with "
"its resize handles, 3D compositions can be created easily:"
msgstr ""

#. (itstool) path: sect1/para
#: tutorial-shapes.xml:267
msgid ""
"Here are some more examples of rectangle compositions, including rounding "
"and gradient fills:"
msgstr ""

#. (itstool) path: sect1/title
#: tutorial-shapes.xml:279
msgid "Ellipses"
msgstr ""

#. (itstool) path: sect1/para
#: tutorial-shapes.xml:280
msgid ""
"The Ellipse tool (<keycap>E</keycap>) can create ellipses and circles, which "
"you can turn into segments or arcs. The drawing shortcuts are the same as "
"those of the rectangle tool:"
msgstr ""

#. (itstool) path: listitem/para
#: tutorial-shapes.xml:286
msgid ""
"With <keycap function=\"control\">Ctrl</keycap>, draw a circle or an integer-"
"ratio (2:1, 3:1, etc.) ellipse."
msgstr ""

#. (itstool) path: sect1/para
#: tutorial-shapes.xml:296
msgid "Let's explore the handles of an ellipse. Select this one:"
msgstr ""

#. (itstool) path: sect1/para
#: tutorial-shapes.xml:306
msgid ""
"Once again, you see three adjustable corner handles initially, but in fact "
"they are four. The rightmost handle is two overlapping handles that let you "
"“open” the ellipse. <mousebutton role=\"mouse-drag\">Drag</mousebutton> that "
"rightmost handle, then drag the other handle which becomes visible under it, "
"to get a variety of pie-chart segments or arcs:"
msgstr ""

#. (itstool) path: sect1/para
#: tutorial-shapes.xml:319
msgid ""
"To get a <firstterm>segment</firstterm> (an arc plus two radii), drag "
"<emphasis>outside</emphasis> the ellipse; to get an <firstterm>arc</"
"firstterm>, drag <emphasis>inside</emphasis> it. Above, there are 4 segments "
"on the left and 3 arcs on the right. Note that arcs are unclosed shapes, i."
"e. the stroke only goes along the ellipse but does not connect the ends of "
"the arc. You can make this obvious if you remove the fill, leaving only "
"stroke:"
msgstr ""

#. (itstool) path: sect1/para
#: tutorial-shapes.xml:332
msgid ""
"Arcs can be closed by a straight line using the button labeled "
"<guimenuitem>Switch to chord (closed shape)</guimenuitem>, though."
msgstr ""

#. (itstool) path: sect1/para
#: tutorial-shapes.xml:336
msgid ""
"Note the fan-like group of narrow segments on the left. It was easy to "
"create using <firstterm>angle snapping</firstterm> of the handle with "
"<keycap function=\"control\">Ctrl</keycap>. Here are the arc/segment handle "
"shortcuts:"
msgstr ""

#. (itstool) path: listitem/para
#: tutorial-shapes.xml:343
msgid ""
"With <keycap function=\"control\">Ctrl</keycap>, snap the handle every 15 "
"degrees when dragging."
msgstr ""

#. (itstool) path: listitem/para
#: tutorial-shapes.xml:348
msgid ""
"<keycombo><keycap function=\"shift\">Shift</keycap><mousebutton "
"role=\"click\">click</mousebutton></keycombo> to make the ellipse whole (not "
"arc or segment)."
msgstr ""

#. (itstool) path: sect1/para
#: tutorial-shapes.xml:354
msgid ""
"The snap angle can be changed in Inkscape Preferences (in "
"<menuchoice><guimenu>Behavior</guimenu><guimenuitem>Steps</guimenuitem></"
"menuchoice>)."
msgstr ""

#. (itstool) path: sect1/para
#: tutorial-shapes.xml:358
msgid ""
"The other two handles of the ellipse are used for resizing it around its "
"center. Their shortcuts are similar to those of the rounding handles of a "
"rectangle:"
msgstr ""

#. (itstool) path: listitem/para
#: tutorial-shapes.xml:364
msgid ""
"<mousebutton role=\"mouse-drag\">Drag</mousebutton> with <keycap "
"function=\"control\">Ctrl</keycap> to make a circle (make the other radius "
"the same)."
msgstr ""

#. (itstool) path: listitem/para
#: tutorial-shapes.xml:370
msgid ""
"<keycombo><keycap function=\"control\">Ctrl</keycap><mousebutton "
"role=\"click\">click</mousebutton></keycombo> to make a circle without "
"dragging."
msgstr ""

#. (itstool) path: sect1/para
#: tutorial-shapes.xml:376
msgid ""
"And, like the rectangle resize handles, these ellipse handles adjust the "
"height and width of the ellipse in <emphasis>the ellipse's own coordinates</"
"emphasis>. This means that a rotated or skewed ellipse can easily be "
"stretched or squeezed along its original axes while remaining rotated or "
"skewed. Try to resize any of these ellipses by their resize handles:"
msgstr ""

#. (itstool) path: sect1/title
#: tutorial-shapes.xml:391
msgid "Stars"
msgstr ""

#. (itstool) path: sect1/para
#: tutorial-shapes.xml:392
msgid ""
"Stars are the most complex and the most exciting Inkscape shape. If you want "
"to wow your friends by Inkscape, let them play with the Star tool. It's "
"endlessly entertaining — outright addictive!"
msgstr ""

#. (itstool) path: sect1/para
#: tutorial-shapes.xml:396
msgid ""
"The Star tool can create two similar but distinct kinds of objects: stars "
"and polygons. A star has two handles whose positions define the length and "
"shape of its tips; a polygon has just one handle which simply rotates and "
"resizes the polygon when dragged:"
msgstr ""

#. (itstool) path: sect1/para
#: tutorial-shapes.xml:408
msgid ""
"In the Controls bar of the Star tool, the first two buttons control how the "
"shape is drawn (regular polygon or star). Next, a numeric field sets the "
"<firstterm>number of vertices</firstterm> of a star or polygon. This "
"parameter is only editable via the Controls bar. The allowed range is from 3 "
"(polygons) or 2 (stars) to 1024."
msgstr ""

#. (itstool) path: sect1/para
#: tutorial-shapes.xml:413
msgid "When drawing a new star or polygon,"
msgstr ""

#. (itstool) path: listitem/para
#: tutorial-shapes.xml:418
msgid ""
"<mousebutton role=\"mouse-drag\">Drag</mousebutton> with <keycap "
"function=\"control\">Ctrl</keycap> to snap the angle to 15 degree increments."
msgstr ""

#. (itstool) path: sect1/para
#: tutorial-shapes.xml:424
msgid ""
"Naturally, a star is a much more interesting shape (though polygons are "
"often more useful in practice). The two handles of a star have slightly "
"different functions. The first handle (initially it is on a vertex, i.e. on "
"a <emphasis>convex</emphasis> corner of the star) makes the star rays longer "
"or shorter, but when you rotate it (relative to the center of the shape), "
"the other handle rotates accordingly. This means you cannot skew the star's "
"rays with this handle."
msgstr ""

#. (itstool) path: sect1/para
#: tutorial-shapes.xml:431
msgid ""
"The other handle (initially in a <emphasis>concave</emphasis> corner between "
"two vertices) is, conversely, free to move both radially and tangentially, "
"without affecting the vertex handle. (In fact, this handle can itself become "
"vertex by moving farther from the center than the other handle.) This is the "
"handle that can skew the star's tips to get all sorts of crystals, mandalas, "
"snowflakes, and porcupines:"
msgstr ""

#. (itstool) path: sect1/para
#: tutorial-shapes.xml:444
msgid ""
"If you want just a plain regular star without any such lacework, you can "
"make the skewing handle behave as the non-skewing one:"
msgstr ""

#. (itstool) path: listitem/para
#: tutorial-shapes.xml:450
msgid ""
"<mousebutton role=\"mouse-drag\">Drag</mousebutton> with <keycap "
"function=\"control\">Ctrl</keycap> to keep the star rays strictly radial (no "
"skew)."
msgstr ""

#. (itstool) path: listitem/para
#: tutorial-shapes.xml:456
msgid ""
"<keycombo><keycap function=\"control\">Ctrl</keycap><mousebutton "
"role=\"click\">click</mousebutton></keycombo> to remove the skew without "
"dragging."
msgstr ""

#. (itstool) path: sect1/para
#: tutorial-shapes.xml:462
msgid ""
"As a useful complement for the on-canvas handle dragging, the Controls bar "
"has the <guimenuitem>Spoke ratio</guimenuitem> field which defines the ratio "
"of the two handles' distances to the center."
msgstr ""

#. (itstool) path: sect1/para
#: tutorial-shapes.xml:466
msgid ""
"Inkscape stars have two more tricks in their bag. In geometry, a polygon is "
"a shape with straight line edges and sharp corners. In the real world, "
"however, various degrees of curvilinearity and roundedness are normally "
"present — and Inkscape can do that too. Rounding a star or polygon works a "
"bit differently from rounding a rectangle, however. You don't use a "
"dedicated handle for this, but"
msgstr ""

#. (itstool) path: listitem/para
#: tutorial-shapes.xml:474
msgid ""
"<keycombo><keycap function=\"shift\">Shift</keycap><mousebutton role=\"mouse-"
"drag\">drag</mousebutton></keycombo> a handle tangentially to round the star "
"or polygon."
msgstr ""

#. (itstool) path: listitem/para
#: tutorial-shapes.xml:480
msgid ""
"<keycombo><keycap function=\"shift\">Shift</keycap><mousebutton "
"role=\"click\">click</mousebutton></keycombo> a handle to remove rounding."
msgstr ""

#. (itstool) path: sect1/para
#: tutorial-shapes.xml:486
msgid ""
"“Tangentially” means in a direction perpendicular to the direction to the "
"center. If you “rotate” a handle with Shift counterclockwise around the "
"center, you get positive roundedness; with clockwise rotation, you get "
"negative roundedness. (See below for examples of negative roundedness.)"
msgstr ""

#. (itstool) path: sect1/para
#: tutorial-shapes.xml:491
msgid ""
"Here's a comparison of a rounded square (Rectangle tool) with a rounded 4-"
"sided polygon (Star tool):"
msgstr ""

#. (itstool) path: sect1/para
#: tutorial-shapes.xml:501
msgid ""
"As you can see, while a rounded rectangle has straight line segments in its "
"sides and circular (generally, elliptic) roundings, a rounded polygon or "
"star has no straight lines at all; its curvature varies smoothly from the "
"maximum (in the corners) to the minimum (mid-way between the corners). "
"Inkscape does this simply by adding collinear Bezier tangents to each node "
"of the shape (you can see them if you convert the shape to path and examine "
"it in Node tool)."
msgstr ""

#. (itstool) path: sect1/para
#: tutorial-shapes.xml:508
msgid ""
"The <guimenuitem>Rounded</guimenuitem> parameter which you can adjust in the "
"Controls bar is the ratio of the length of these tangents to the length of "
"the polygon/star sides to which they are adjacent. This parameter can be "
"negative, which reverses the direction of tangents. The values of about 0.2 "
"to 0.4 give “normal” rounding of the kind you would expect; other values "
"tend to produce beautiful, intricate, and totally unpredictable patterns. A "
"star with a large roundedness value may reach far beyond the positions of "
"its handles. Here are a few examples, each indicating its roundedness value:"
msgstr ""

#. (itstool) path: sect1/para
#: tutorial-shapes.xml:523
msgid ""
"If you want the tips of a star to be sharp but the concaves smooth or vice "
"versa, this is easy to do by creating an <firstterm>offset</firstterm> "
"(<keycombo><keycap function=\"control\">Ctrl</keycap><keycap>J</keycap></"
"keycombo>) from the star:"
msgstr ""

#. (itstool) path: sect1/para
#: tutorial-shapes.xml:535
msgid ""
"<keycombo><keycap function=\"shift\">Shift</keycap><mousebutton role=\"mouse-"
"drag\">drag</mousebutton></keycombo>ging star handles in Inkscape is one of "
"the finest pursuits known to man. But it can get better still."
msgstr ""

#. (itstool) path: sect1/para
#: tutorial-shapes.xml:539
msgid ""
"To closer imitate real world shapes, Inkscape can <firstterm>randomize</"
"firstterm> (i.e. randomly distort) its stars and polygons. Slight "
"randomization makes a star less regular, more humane, often funny; strong "
"randomization is an exciting way to obtain a variety of crazily "
"unpredictable shapes. A rounded star remains smoothly rounded when "
"randomized. Here are the shortcuts:"
msgstr ""

#. (itstool) path: listitem/para
#: tutorial-shapes.xml:547
msgid ""
"<keycombo><keycap function=\"alt\">Alt</keycap><mousebutton role=\"mouse-"
"drag\">drag</mousebutton></keycombo> a handle tangentially to randomize the "
"star or polygon."
msgstr ""

#. (itstool) path: listitem/para
#: tutorial-shapes.xml:553
msgid ""
"<keycombo><keycap function=\"alt\">Alt</keycap><mousebutton "
"role=\"click\">click</mousebutton></keycombo> a handle to remove "
"randomization."
msgstr ""

#. (itstool) path: sect1/para
#: tutorial-shapes.xml:559
msgid ""
"As you draw or handle-drag-edit a randomized star, it will “tremble” because "
"each unique position of its handles corresponds to its own unique "
"randomization. So, moving a handle without Alt re-randomizes the shape at "
"the same randomization level, while Alt-dragging it keeps the same "
"randomization but adjusts its level. Here are stars whose parameters are "
"exactly the same, but each one is re-randomized by very slightly moving its "
"handle (randomization level is 0.1 throughout):"
msgstr ""

#. (itstool) path: sect1/para
#: tutorial-shapes.xml:573
msgid ""
"And here is the middle star from the previous row, with the randomization "
"level varying from -0.2 to 0.2:"
msgstr ""

#. (itstool) path: sect1/para
#: tutorial-shapes.xml:583
msgid ""
"<keycombo><keycap function=\"alt\">Alt</keycap><mousebutton role=\"mouse-"
"drag\">drag</mousebutton></keycombo> a handle of the middle star in this row "
"and observe as it morphs into its neighbors on the right and left — and "
"beyond."
msgstr ""

#. (itstool) path: sect1/para
#: tutorial-shapes.xml:588
msgid ""
"You will probably find your own applications for randomized stars, but I am "
"especially fond of rounded amoeba-like blotches and large roughened planets "
"with fantastic landscapes:"
msgstr ""

#. (itstool) path: sect1/title
#: tutorial-shapes.xml:601
msgid "Spirals"
msgstr ""

#. (itstool) path: sect1/para
#: tutorial-shapes.xml:602
msgid ""
"Inkscape's spiral is a versatile shape, and though not as immersing as the "
"star, it is sometimes very useful. A spiral, like a star, is drawn from the "
"center; while drawing as well as while editing,"
msgstr ""

#. (itstool) path: listitem/para
#: tutorial-shapes.xml:608
msgid ""
"<keycombo><keycap function=\"control\">Ctrl</keycap><mousebutton "
"role=\"mouse-drag\">drag</mousebutton></keycombo> to snap angle to 15 degree "
"increments."
msgstr ""

#. (itstool) path: sect1/para
#: tutorial-shapes.xml:614
msgid ""
"Once drawn, a spiral has two handles at its inner and outer ends. Both "
"handles, when simply dragged, roll or unroll the spiral (i.e. “continue” it, "
"changing the number of turns). Other shortcuts:"
msgstr ""

#. (itstool) path: sect1/para
#: tutorial-shapes.xml:618
msgid "Outer handle:"
msgstr ""

#. (itstool) path: listitem/para
#: tutorial-shapes.xml:623
msgid ""
"<keycombo><keycap function=\"shift\">Shift</keycap><mousebutton role=\"mouse-"
"drag\">drag</mousebutton></keycombo> to scale/rotate around center (no "
"rolling/unrolling)."
msgstr ""

#. (itstool) path: listitem/para
#: tutorial-shapes.xml:629
msgid ""
"<keycombo><keycap function=\"alt\">Alt</keycap><mousebutton role=\"mouse-"
"drag\">drag</mousebutton></keycombo> to lock radius while rolling/unrolling."
msgstr ""

#. (itstool) path: sect1/para
#: tutorial-shapes.xml:635
msgid "Inner handle:"
msgstr ""

#. (itstool) path: listitem/para
#: tutorial-shapes.xml:640
msgid ""
"<keycombo><keycap function=\"alt\">Alt</keycap><mousebutton role=\"mouse-"
"drag\">drag</mousebutton></keycombo> vertically to converge/diverge."
msgstr ""

#. (itstool) path: listitem/para
#: tutorial-shapes.xml:646
msgid ""
"<keycombo><keycap function=\"alt\">Alt</keycap><mousebutton "
"role=\"click\">click</mousebutton></keycombo> to reset divergence."
msgstr ""

#. (itstool) path: listitem/para
#: tutorial-shapes.xml:652
msgid ""
"<keycombo><keycap function=\"shift\">Shift</keycap><mousebutton "
"role=\"click\">click</mousebutton></keycombo> to move the inner handle to "
"the center."
msgstr ""

#. (itstool) path: sect1/para
#: tutorial-shapes.xml:658
msgid ""
"The <firstterm>divergence</firstterm> of a spiral is the measure of "
"nonlinearity of its winds. When it is equal to 1, the spiral is uniform; "
"when it is less than 1 (<keycombo><keycap function=\"alt\">Alt</"
"keycap><mousebutton role=\"mouse-drag\">drag</mousebutton></keycombo> "
"upwards), the spiral is denser on the periphery; when it is greater than 1 "
"(<keycombo><keycap function=\"alt\">Alt</keycap><mousebutton role=\"mouse-"
"drag\">drag</mousebutton></keycombo> downwards), the spiral is denser "
"towards the center:"
msgstr ""

#. (itstool) path: sect1/para
#: tutorial-shapes.xml:673
msgid "The maximum number of spiral turns is 1024."
msgstr ""

#. (itstool) path: sect1/para
#: tutorial-shapes.xml:676
msgid ""
"Just as the Ellipse tool is good not only for ellipses but also for arcs "
"(lines of constant curvature), the Spiral tool is useful for making curves "
"of <emphasis>smoothly varying</emphasis> curvature. Compared to a plain "
"Bezier curve, an arc or a spiral is often more convenient because you can "
"make it shorter or longer by dragging a handle along the curve without "
"affecting its shape. Also, while a spiral is normally drawn without fill, "
"you can add fill and remove stroke for interesting effects."
msgstr ""

#. (itstool) path: sect1/para
#: tutorial-shapes.xml:690
msgid ""
"Especially interesting are spirals with dotted stroke — they combine the "
"smooth concentration of the shape with regular equispaced marks (dots or "
"dashes) for beautiful moire effects:"
msgstr ""

#. (itstool) path: sect1/title
#: tutorial-shapes.xml:703
msgid "Conclusion"
msgstr ""

#. (itstool) path: sect1/para
#: tutorial-shapes.xml:704
msgid ""
"Inkscape's shape tools are very powerful. Learn their tricks and play with "
"them at your leisure — this will pay off when you do your design work, "
"because using shapes instead of simple paths often makes vector art faster "
"to create and easier to modify. If you have any ideas for further shape "
"improvements, please contact the developers."
msgstr ""

#. (itstool) path: Work/format
#: shapes-f01.svg:48 shapes-f02.svg:49 shapes-f03.svg:48 shapes-f04.svg:48
#: shapes-f05.svg:48 shapes-f06.svg:49 shapes-f07.svg:49 shapes-f08.svg:695
#: shapes-f09.svg:48 shapes-f10.svg:48 shapes-f11.svg:48 shapes-f12.svg:48
#: shapes-f13.svg:49 shapes-f14.svg:48 shapes-f15.svg:49 shapes-f16.svg:48
#: shapes-f17.svg:50 shapes-f18.svg:48 shapes-f19.svg:48 shapes-f20.svg:48
#: shapes-f21.svg:48 shapes-f22.svg:48 shapes-f23.svg:48
msgid "image/svg+xml"
msgstr ""

#. (itstool) path: text/tspan
#: shapes-f01.svg:69
#, no-wrap
msgid "draw here"
msgstr ""

#. (itstool) path: text/tspan
#: shapes-f02.svg:70
#, no-wrap
msgid "Elliptic rounded corners"
msgstr ""

#. (itstool) path: text/tspan
#: shapes-f02.svg:81
#, no-wrap
msgid "Circular rounded corners"
msgstr ""

#. (itstool) path: text/tspan
#: shapes-f03.svg:69 shapes-f04.svg:69
#, no-wrap
msgid "Scaling rounded rectangles "
msgstr ""

#. (itstool) path: text/tspan
#: shapes-f03.svg:74
#, no-wrap
msgid "with \"Scale rounded corners\" OFF"
msgstr ""

#. (itstool) path: text/tspan
#: shapes-f04.svg:74
#, no-wrap
msgid "with \"Scale rounded corners\" ON"
msgstr ""

#. (itstool) path: text/tspan
#: shapes-f06.svg:110
#, no-wrap
msgid "Snapping of rectangle "
msgstr ""

#. (itstool) path: text/tspan
#: shapes-f06.svg:114
#, no-wrap
msgid "- resize handles with Ctrl"
msgstr ""

#. (itstool) path: text/tspan
#: shapes-f07.svg:229
#, no-wrap
msgid "3 original rectangles"
msgstr ""

#. (itstool) path: text/tspan
#: shapes-f07.svg:239
#, no-wrap
msgid "Several rectangles copied and resized by handles"
msgstr ""

#. (itstool) path: text/tspan
#: shapes-f07.svg:243
#, no-wrap
msgid "(mostly with Ctrl)"
msgstr ""

#. (itstool) path: text/tspan
#: shapes-f11.svg:101
#, no-wrap
msgid "15"
msgstr ""

#. (itstool) path: text/tspan
#: shapes-f11.svg:120
#, no-wrap
msgid "Segments"
msgstr ""

#. (itstool) path: text/tspan
#: shapes-f11.svg:131
#, no-wrap
msgid "Arcs"
msgstr ""

#. (itstool) path: text/tspan
#: shapes-f13.svg:90
#, no-wrap
msgid "Star"
msgstr ""

#. (itstool) path: text/tspan
#: shapes-f13.svg:122
#, no-wrap
msgid "Polygon"
msgstr ""

#. (itstool) path: text/tspan
#: shapes-f15.svg:89 shapes-f15.svg:117
#, no-wrap
msgid "Rounded"
msgstr ""

#. (itstool) path: text/tspan
#: shapes-f15.svg:93
#, no-wrap
msgid "polygon"
msgstr ""

#. (itstool) path: text/tspan
#: shapes-f15.svg:121
#, no-wrap
msgid "rectangle"
msgstr ""

#. (itstool) path: text/tspan
#: shapes-f16.svg:276 shapes-f16.svg:287 shapes-f16.svg:298
#, no-wrap
msgid "0.25"
msgstr ""

#. (itstool) path: text/tspan
#: shapes-f16.svg:310
#, no-wrap
msgid "0.37"
msgstr ""

#. (itstool) path: text/tspan
#: shapes-f16.svg:337
#, no-wrap
msgid "0.43"
msgstr ""

#. (itstool) path: text/tspan
#: shapes-f16.svg:349 shapes-f16.svg:357 shapes-f16.svg:425
#, no-wrap
msgid "3.00"
msgstr ""

#. (itstool) path: text/tspan
#: shapes-f16.svg:384
#, no-wrap
msgid "0.41"
msgstr ""

#. (itstool) path: text/tspan
#: shapes-f16.svg:395
#, no-wrap
msgid "5.43"
msgstr ""

#. (itstool) path: text/tspan
#: shapes-f16.svg:406
#, no-wrap
msgid "1.85"
msgstr ""

#. (itstool) path: text/tspan
#: shapes-f16.svg:417
#, no-wrap
msgid "0.21"
msgstr ""

#. (itstool) path: text/tspan
#: shapes-f16.svg:457
#, no-wrap
msgid "−0.43"
msgstr ""

#. (itstool) path: text/tspan
#: shapes-f16.svg:484
#, no-wrap
msgid "−8.94"
msgstr ""

#. (itstool) path: text/tspan
#: shapes-f16.svg:510
#, no-wrap
msgid "0.39"
msgstr ""

#. (itstool) path: text/tspan
#: shapes-f17.svg:105
#, no-wrap
msgid "Original star"
msgstr ""

#. (itstool) path: text/tspan
#: shapes-f17.svg:115
#, no-wrap
msgid "Linked offset, inset"
msgstr ""

#. (itstool) path: text/tspan
#: shapes-f17.svg:125
#, no-wrap
msgid "Linked offset, outset"
msgstr ""

#. (itstool) path: text/tspan
#: shapes-f19.svg:69
#, no-wrap
msgid "+0.2"
msgstr ""

#. (itstool) path: text/tspan
#: shapes-f19.svg:80
#, no-wrap
msgid "+0.1"
msgstr ""

#. (itstool) path: text/tspan
#: shapes-f19.svg:91
#, no-wrap
msgid "0"
msgstr ""

#. (itstool) path: text/tspan
#: shapes-f19.svg:102
#, no-wrap
msgid "−0.1"
msgstr ""

#. (itstool) path: text/tspan
#: shapes-f19.svg:113
#, no-wrap
msgid "−0.2"
msgstr ""

#. (itstool) path: text/tspan
#: shapes-f21.svg:69
#, no-wrap
msgid "0.2"
msgstr ""

#. (itstool) path: text/tspan
#: shapes-f21.svg:80
#, no-wrap
msgid "0.5"
msgstr ""

#. (itstool) path: text/tspan
#: shapes-f21.svg:91
#, no-wrap
msgid "6"
msgstr ""

#. (itstool) path: text/tspan
#: shapes-f21.svg:102
#, no-wrap
msgid "2"
msgstr ""

#. (itstool) path: text/tspan
#: shapes-f21.svg:113
#, no-wrap
msgid "1"
msgstr ""
